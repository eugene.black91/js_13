const images = document.querySelectorAll('.image-to-show');
const firstImage = images[0];
const lastImage = images[images.length - 1];
const stopButton = document.querySelector('.stop');
const starButton = document.querySelector('.start');

const slider = () => {
    const currentImage = document.querySelector('.show');
    if(currentImage !== lastImage){
        currentImage.classList.remove('show');
        currentImage.nextElementSibling.classList.add('show');
    }else{
        currentImage.classList.remove('show');
        firstImage.classList.add('show');
    }
}

let timer = setInterval(slider, 3000);

stopButton.addEventListener('click', () => {
    clearInterval(timer);
    starButton.disabled = false;
    stopButton.disabled = true;

})

starButton.addEventListener('click', () => {
    timer = setInterval(slider, 3000);
    starButton.disabled = true;
    stopButton.disabled = false;
})